extends Node

@onready var ui = %UI
@onready var sound = $PickupSound
@onready var player = %Player

var coin = 0;
var egg = 0;

func pickup(item: String) -> void:
	if item == "egg":
		add_egg()
	elif item == "something":
		player.spawn_text("Commerce!")
	elif item == "playcube":
		player.current_direction = "down"
		player.spawn_text("YOU WIN!",false)
		%Player/Camera2D.zoom.x = 2
		%Player/Camera2D.zoom.y = 2
		sound = $VictorySound
	ui.update()
	sound.play()

func add_egg(val = 1):
	egg += val
	if val == 1:
		player.spawn_text("+" + str(val) + " egg")
	else:
		player.spawn_text("+" + str(val) + " eggs")

func add_coin(val = 1):
	coin += val
	if val == 1:
		player.spawn_text("+" + str(val) + " coin")
	else:
		player.spawn_text("+" + str(val) + " coins")


func get_eggs() -> int:
	return egg

func get_coins() -> int:
	return coin

func remove_coin(val = 1) -> void:
	if val <= coin:
		coin -= val
		if val == 1:
			player.spawn_text("-" + str(val) + " coin")
		else:
			player.spawn_text("-" + str(val) + " coins")
	else:
		print("Someone stole your money. Crashing immediately.")
		breakpoint
	ui.update()

func remove_egg(val = 1) -> void:
	if val <= egg:
		egg -= val
		if val == 1:
			player.spawn_text("-" + str(val) + " egg")
		else:
			player.spawn_text("-" + str(val) + " eggs")
	else:
		print("Someone stole your eggs. Crashing immediately.")
		breakpoint
	ui.update()
