extends StaticBody2D

@onready var game_manager = get_tree().get_first_node_in_group("GameManager")
const EGG_PRICE = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


func _on_trigger_zone_body_entered(_body):
	var eggs = game_manager.get_eggs()
	if eggs == 0:
		game_manager.player.spawn_text("No egg to sell")
	else:
		game_manager.remove_egg(eggs)
		game_manager.add_coin(eggs * EGG_PRICE)
	pass # Replace with function body.
