extends VBoxContainer

@onready var game_manager = get_tree().get_first_node_in_group("GameManager")
@export var item_cost = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	var player_coins = game_manager.get_coins()
	if player_coins >= item_cost:
		$ItemButton.disabled = false
	else:
		$ItemButton.disabled = true
	$ItemButton.text = str(item_cost)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_visibility_changed():
	var player_coins = game_manager.get_coins()
	if player_coins >= item_cost:
		$ItemButton.disabled = false
	else:
		$ItemButton.disabled = true
	$ItemButton.text = str(item_cost)
