extends StaticBody2D

@onready var shop = $ShopDisplay
@onready var game_manager = get_tree().get_first_node_in_group("GameManager")


func _ready():
	# Make sure shop is hidden on load
	shop.visible = false

func _on_trigger_zone_body_entered(_body):
	shop.visible = true

func _on_button_pressed():
	shop.visible = false

func _on_item_button_pressed():
	game_manager.pickup("something")
	shop.visible = false


func _on_item_button_pressed_playcube():
	game_manager.pickup("playcube")
	game_manager.remove_coin(500)
	shop.visible = false
