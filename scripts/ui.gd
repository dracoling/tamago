extends CanvasLayer

@export var game_manager: Node

var display_coin = true
var display_egg = true
@onready var egg_image = %Scoreboard/GridContainer/EggImage
@onready var egg_label = %Scoreboard/GridContainer/EggLabel
@onready var coin_label = %Scoreboard/GridContainer/CoinLabel
@onready var coin_image = %Scoreboard/GridContainer/CoinImage
const FRAMES_PER_UPDATE = 10
var need_update = FRAMES_PER_UPDATE

func _ready():
	update()

func _process(_delta):
	need_update -= 1
	if need_update <= 0:
		need_update = FRAMES_PER_UPDATE
		var coins = game_manager.get_coins()
		var eggs = game_manager.get_eggs()

		egg_image.visible = display_egg
		egg_label.visible = display_egg
		egg_label.text = str(eggs)
		coin_image.visible = display_coin
		coin_label.visible = display_coin
		coin_label.text = str(coins)

func update():
	need_update = 0
