extends CharacterBody2D


@export var SPEED = 400.0
const JUMP_VELOCITY = -400.0
var current_direction = '';
var float_text_pool:Array[FloatText2D] = []
@onready var float_text_template = preload("res://scenes/float_text_2d.tscn")


func _physics_process(delta):
	player_movement(delta)

func player_movement(_delta):
	var movement = 1
	velocity.x = 0;
	velocity.y = 0;
	if Input.is_action_pressed("ui_right") or Input.is_action_pressed("move_right"):
		current_direction = "right";
		velocity.x += SPEED;
	if Input.is_action_pressed("ui_left") or Input.is_action_pressed("move_left"):
		current_direction = "left"
		velocity.x -= SPEED;
	if Input.is_action_pressed("ui_up") or Input.is_action_pressed("move_up"):
		current_direction = "up";
		velocity.y -= SPEED;
	if Input.is_action_pressed("ui_down") or Input.is_action_pressed("move_down"):
		current_direction = "down";
		velocity.y += SPEED;
	if velocity.x == 0 && velocity.y == 0:
		movement = 0;
	else:
		velocity = velocity.limit_length(SPEED)
		movement = 1;

	play_anim(movement)
	move_and_slide()

func run_anim(animation):
	var anim = $AnimatedSprite2D
	anim.play(animation)

func play_anim(movement):
	var dir = current_direction
	var anim = $AnimatedSprite2D
	
	if dir == "right":
		if movement == 1:
			anim.play("run_right")
		elif movement == 0:
			anim.play("idle_right")
	elif dir == "left":
		if movement == 1:
			anim.play("run_left")
		elif movement == 0:
			anim.play("idle_left")
	elif dir == "up":
		if movement == 1:
			anim.play("run_up")
		elif movement == 0:
			anim.play("idle_up")
	elif dir == "down":
		if movement == 1:
			anim.play("run_down")
		elif movement == 0:
			anim.play("idle_down")

func spawn_text(value:String,animate = true):
	var float_text = get_float_text()
	var pos = $TextPosition2D.position
	var height = 64
	var spread = 128
	add_child(float_text, true)
	if animate:
		float_text.set_values_and_animate(value,pos,height,spread)
	else:
		float_text.hover(value,pos)
	#breakpoint

func get_float_text() -> FloatText2D:
	if float_text_pool.size() > 0:
		return float_text_pool.pop_front()
	else: 
		var new_float_text = float_text_template.instantiate()
		new_float_text.tree_exiting.connect(
			func():float_text_pool.append(new_float_text))
		return new_float_text
